//#pragma GCC optimize("Os")

#include <WiFi.h>
#include <Firebase_ESP_Client.h>
#include <addons/TokenHelper.h>
#include <addons/RTDBHelper.h>

#define WIFI_SSID "" //nome do wifi
#define WIFI_PASSWORD "" //senha do wifi
#define API_KEY "AIzaSyDMDMOEk70jqCIoIbY4mHI64k8uUWBeEtQ"
#define USER_EMAIL "gabrielm.pedroza@usp.br"
#define USER_PASSWORD "esp32toroff"
#define DATABASE_URL "https://toroff-eb183-default-rtdb.firebaseio.com/"
#define TOROFFNUMBER "0"

#define BUTTON_PIN 15
#define RAIN_POWER 27
#define RAIN_OUTPUT 32

//configuracao do pwm do motor
#define MOTOR_A 13
#define MOTOR_B 12
#define FREQ 5000
#define CHANNEL_A 0
#define CHANNEL_B 1
#define RES 8
#define POT_MAX 100

//variaveis pro firebase
FirebaseData fbdo;
FirebaseAuth auth;
FirebaseConfig config;

//variaveis uteis para o funcionamento
int state = 0;
int previousCloud = 0;
int wetness = 0;
String family = "1";
String familyRoot = "/Family/";
String dataRoot = "/teste02/";
String uid = "";

//essas flags sao usadas para comunicar os diferentes cores da esp32
struct Flags {
    bool getState;
    bool setState;
    bool setWetness;
};

const Flags defaultFlags = {0, 0, 0};

//variaveis pra lidar com os varios cores
TaskHandle_t cloudStuff, circuitStuff;
QueueHandle_t flagsChannel, intChannel;

/*  void handleSendStr(String thing) {
    String* pointer = new String(thing);
    xQueueSend(stringChannel, &pointer, 0);
    }
    String handleReceiveStr(Flags flags) {
    String* resultPointer;
    xQueueSend(flagsChannel, &flags, 0);

    xQueueReceive(stringChannel, &resultPointer, portMAX_DELAY);
    String result = *resultPointer;
    delete resultPointer;
    return result;
    }*/

//roda no core 0, servicos de nuvem
void cloudCode(void * parameter) {
    setWetness();
    while (1) {
        //recebe as flags enviadas pelo core 1
        Flags flags = defaultFlags;
        xQueueReceive(flagsChannel, &flags, portMAX_DELAY);
        if (flags.getState) {
            //responde com o estado
            int ans = getState();
            xQueueSend(intChannel, &ans, 0);
        }
        if (flags.setState) {
            setState();
        }
        if (flags.setWetness) {
            setWetness();
        }
    }

}
void circuitCode(void * parameter) {
    uint64_t timeRain = millis();
    while (1) {
        if(millis() > 60*1000) ESP.restart();
        Flags flags = defaultFlags;
        state = digitalRead(BUTTON_PIN);

        if (millis() - timeRain > 10 * 1000) {
            digitalWrite(RAIN_POWER, HIGH);
            delay(100);
            int val = analogRead(RAIN_OUTPUT);
            if (val < 3000) {
                Serial.println("wett");
                wetness = 1;
                flags.setWetness = 1;
                xQueueSend(flagsChannel, &flags, 0);
                flags = defaultFlags;

                /*  flags.getState = 1;
                    int temp;
                    xQueueSend(flagsChannel, &flags, 0);
                    xQueueReceive(intChannel, &cs, portMAX_DELAY);
                    flags = defaultFlags;*/
            } else {
                wetness = 0;
                flags.setWetness = 1;
                xQueueSend(flagsChannel, &flags, 0);
                flags = defaultFlags;

                /*  flags.getState = 1;
                    int temp;
                    xQueueSend(flagsChannel, &flags, 0);
                    xQueueReceive(intChannel, &cs, portMAX_DELAY);
                    flags = defaultFlags;*/
            }
            digitalWrite(RAIN_POWER, LOW);
            timeRain = millis();
        }
        //pede o estado da nuvem pro outro core
        flags.getState = 1;
        int cs;
        xQueueSend(flagsChannel, &flags, 0);
        if(xQueueReceive(intChannel, &cs, 5000/portTICK_PERIOD_MS) == pdFALSE) ESP.restart();
        flags = defaultFlags;
        if (previousCloud != cs && cs != state) {
            //mudaram o app e ta diferente da janela fisica
            if (cs == 0) {
                //eh pra abrir
                moveMotor(cs, state);
                delay(1000);
            }
            do { //move o motor ate ajustar
                //a parte dos ifs é pra pegar uma mudanca do estado no server
                if (flags.getState == 0) {
                    flags.getState = 1;
                    xQueueSend(flagsChannel, &flags, 0);
                }
                if (xQueueReceive(intChannel, &cs, 0) != pdFALSE) {
                    flags = defaultFlags;
                }
                state = digitalRead(BUTTON_PIN);
                moveMotor(cs, state);
            } while (cs != state);
            if (flags.getState == 1) {//se ele pediu uma atualizacao do estado mas saiu do loop antes de recebe-la
                if(xQueueReceive(intChannel, &cs, 5000/portTICK_PERIOD_MS) == pdFALSE) ESP.restart();
                flags = defaultFlags;
            }
            previousCloud = cs;
        } else if (previousCloud == cs && cs != state) {
            //nuvem continua a mesma, mas mudaram o valor da janela na mao
            flags.setState = 1;
            xQueueSend(flagsChannel, &flags, 0);
            flags = defaultFlags;

            flags.getState = 1;
            xQueueSend(flagsChannel, &flags, 0);
            if(xQueueReceive(intChannel, &cs, 5000/portTICK_PERIOD_MS) == pdFALSE) ESP.restart();
            flags = defaultFlags;
            previousCloud = cs;
        } else if (previousCloud == cs && wetness == 1 && state != 1) {
            //nao mudaram o app e nao mexeram na janela, mas ta molhado e a janela ta aberta
            //Serial.println("hi");
            do { //move o motor ate ajustar
                state = digitalRead(BUTTON_PIN);
                moveMotor(wetness, state);
            } while (wetness != state);
            //fechou a janela
            flags.setState = 1;
            xQueueSend(flagsChannel, &flags, 0);
            flags = defaultFlags;
            //dar um tempo até a próxima
            flags.getState = 1;
            xQueueSend(flagsChannel, &flags, 0);
            if(xQueueReceive(intChannel, &cs, 5000/portTICK_PERIOD_MS) == pdFALSE) ESP.restart();
            flags = defaultFlags;
            previousCloud = cs;
        }
    }
}

void moveMotor(bool cs, bool es) {

    int cs_i = cs;
    int es_i = es;

    int diff = (cs_i - es_i);
    int valA = abs(diff) * POT_MAX * (diff + 1) / 2;
    int valB = abs(diff) * POT_MAX * (1 - diff) / 2;
    //cs_i es_i valA val B
    //1    1    0    0
    //0    0    0    0
    //1    0    125  0
    //0    1    0    125

    ledcWrite(CHANNEL_A, valA);
    ledcWrite(CHANNEL_B, valB);
}

void iniciarWifi() {
    WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
    Serial.print("Connecting to Wi-Fi");
    while (WiFi.status() != WL_CONNECTED)
    {
        Serial.print(".");
        delay(300);
    }
    Serial.println();
    Serial.print("Connected with IP: ");
    Serial.println(WiFi.localIP());
    Serial.println();
}

void iniciarFirebase() {

    config.api_key = API_KEY;
    auth.user.email = USER_EMAIL;
    auth.user.password = USER_PASSWORD;
    config.database_url = DATABASE_URL;
    Firebase.reconnectNetwork(true);
    fbdo.setBSSLBufferSize(4096, 1024);

    fbdo.setResponseSize(4096);

    config.token_status_callback = tokenStatusCallback;
    Firebase.begin(&config, &auth);
}

bool setString(String path, String val) {
    while (!Firebase.ready());
    bool res = Firebase.RTDB.setString(&fbdo, path, val);
    return res;
}
bool setInt(String path, int val) {
    while (!Firebase.ready());
    bool res = Firebase.RTDB.setInt(&fbdo, path, val);
    return res;
}
String getFamily() {
    while (!Firebase.ready());
    String fr = familyRoot + uid;
    if (!Firebase.RTDB.getString(&fbdo, fr)) {
        setString(fr, family);
        getFamily();
    }
    return family = fbdo.to<String>();
}

String getClimate() {
    while (!Firebase.ready());
    String dr = dataRoot + "/" + family + "/climate/";
    if (!Firebase.RTDB.getString(&fbdo, dr)) {
        setString(dr, "climatePlaceholder");
        getClimate();
    }
    return fbdo.to<String>();
}

int getState() {
    while (!Firebase.ready());
    String sr = dataRoot + "/" + "/state/";
    if (!Firebase.RTDB.getString(&fbdo, sr)) {
        setInt(sr, state);
        getState();
    }
    return fbdo.to<String>().toInt();
}

void setState() {
    String sr = dataRoot + "/" + "/state/";
    setString(sr, String(state));

}

void setWetness() {
    String sr = dataRoot + "/" +  "/wetness/";
    setInt(sr, wetness);

}

void setup()
{
    Serial.begin(115200);
    //setup do botao e do sensor de chuva
    pinMode(BUTTON_PIN, INPUT_PULLDOWN);
    pinMode(RAIN_OUTPUT, INPUT);
    analogRead(RAIN_OUTPUT); //cleaning the first read
    pinMode(RAIN_POWER, OUTPUT);

    //setup do motor
    pinMode(MOTOR_A, OUTPUT);
    pinMode(MOTOR_B, OUTPUT);
    ledcSetup(CHANNEL_A, FREQ, RES);
    ledcSetup(CHANNEL_B, FREQ, RES);

    ledcAttachPin(MOTOR_A, CHANNEL_A);
    ledcAttachPin(MOTOR_B, CHANNEL_B);

    iniciarWifi();

    iniciarFirebase();

    uid = auth.token.uid.c_str();

    family = getFamily();

    //setup dos treco do processamento paralelo
    flagsChannel = xQueueCreate(75, sizeof(Flags));
    intChannel = xQueueCreate(75, sizeof(int));

    xTaskCreatePinnedToCore(
        cloudCode,
        "cloudStuff",
        10000,
        NULL,
        0,
        &cloudStuff,
        0); //roda no core 0

    xTaskCreatePinnedToCore(
        circuitCode,
        "circuitStuff",
        10000,
        NULL,
        1,
        &circuitStuff,
        1); //roda no core 1

}

void loop() {}

